<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Cbf\Checkout\AwareCheckout;
use Cbf\Event\GetTotal;
use Cbf\EventDispatcher;
use Cbf\Listener\BulkTshirtSale;
use Cbf\Listener\TwoForOne;
use Cbf\Registry\ItemRegistryFactory;

$eventDispatcher = EventDispatcher::getInstance();
$eventDispatcher->register(GetTotal::class, new TwoForOne());
$eventDispatcher->register(GetTotal::class, new BulkTshirtSale());

$checkout = new AwareCheckout(ItemRegistryFactory::create());
$checkout->scan('VOUCHER');
$checkout->scan('TSHIRT');
$checkout->scan('MUG');

echo $checkout . PHP_EOL;

$checkout->clear();
$checkout->scan('VOUCHER');
$checkout->scan('TSHIRT');
$checkout->scan('VOUCHER');

echo $checkout . PHP_EOL;

$checkout->clear();
$checkout->scan('TSHIRT');
$checkout->scan('TSHIRT');
$checkout->scan('TSHIRT');
$checkout->scan('VOUCHER');
$checkout->scan('TSHIRT');

echo $checkout . PHP_EOL;

$checkout->clear();
$checkout->scan('VOUCHER');
$checkout->scan('TSHIRT');
$checkout->scan('VOUCHER');
$checkout->scan('VOUCHER');
$checkout->scan('MUG');
$checkout->scan('TSHIRT');
$checkout->scan('TSHIRT');

echo $checkout;
