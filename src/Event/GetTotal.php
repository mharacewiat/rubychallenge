<?php

namespace Cbf\Event;

use Cbf\CheckoutInterface;
use Cbf\EventDispatcher\Event;

/**
 * GetTotal class.
 *
 * @package Cbf\Event
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class GetTotal extends Event
{

    /**
     * @var CheckoutInterface
     */
    protected $checkout;


    /**
     * GetTotal constructor.
     *
     * @param CheckoutInterface $checkout
     */
    public function __construct(CheckoutInterface $checkout)
    {
        $this->checkout = $checkout;
    }


    /**
     * @return CheckoutInterface
     */
    public function getCheckout(): CheckoutInterface
    {
        return $this->checkout;
    }

    /**
     * @param string $lookup
     * @return int
     */
    public function countItem(string $lookup): int
    {
        /**
         * @param string $item
         * @return bool
         */
        $filter = function (string $item) use ($lookup): bool {
            return ($lookup == $item);
        };

        return count(array_filter($this->checkout->getItems(), $filter));
    }

    /**
     * @param float $amount
     */
    public function substituteTotal(float $amount): void
    {
        $this->checkout->substituteTotal($amount);
        return;
    }

}
