<?php

namespace Cbf\Checkout;

use Cbf\Checkout;
use Cbf\Event\GetTotal;
use Cbf\EventDispatcher\EventDispatcherAware;
use Cbf\Registry\RegistryAware;
use Cbf\RegistryInterface;

/**
 * AwareCheckout class.
 *
 * @package Cbf\Checkout
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class AwareCheckout extends Checkout
{

    use EventDispatcherAware, RegistryAware;


    /**
     * AwareCheckout constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        $this->registry = $registry;
    }


    /**
     * {@inheritdoc}
     */
    public function scan(string $item): bool
    {
        try {
            [, $price] = $this->getRegistry()->get($item);
            $this->items[] = $item;
            $this->total += $price;
        } catch (\Exception $exception) {
            return false;
        }

        return true;
    }

    /**
     * @return float
     */
    public function getTotal(): float
    {
        $this->dispatch(new GetTotal($this));
        return parent::getTotal();
    }

}
