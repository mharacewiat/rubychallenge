<?php

namespace Cbf;

/**
 * CheckoutInterface interface.
 *
 * @package Cbf
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
interface CheckoutInterface
{

    /**
     * Add item to ccheckout.
     *
     * @param string $item
     * @return bool
     */
    public function scan(string $item): bool;

    /**
     * @return iterable
     */
    public function getItems(): iterable;

    /**
     * Reduce total by given amount.
     *
     * @param float $amount
     * @return CheckoutInterface
     */
    public function substituteTotal(float $amount): CheckoutInterface;


    /**
     * @return float
     */
    public function getTotal(): float;

}
