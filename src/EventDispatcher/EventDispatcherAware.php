<?php

namespace Cbf\EventDispatcher;

use Cbf\EventDispatcher;

/**
 * EventDispatcherAware trait.
 *
 * @package Cbf\EventDispatcher
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
trait EventDispatcherAware
{

    /**
     * Shortcut for dispatching events.
     *
     * @param EventInterface $event
     */
    protected function dispatch(EventInterface $event): void
    {
        EventDispatcher::getInstance()->dispatch($event);
        return;
    }

}
