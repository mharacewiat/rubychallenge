<?php

namespace Cbf\EventDispatcher;

/**
 * EventInterface interface.
 *
 * @package Cbf\EventDispatcher
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
abstract class Event implements EventInterface
{

    /**
     * @var bool
     */
    protected $propagationStopped = false;


    /**
     * {@inheritdoc}
     */
    public function getName(): ?string
    {
        return null;
    }


    /**
     * @param bool $propagationStopped
     * @return EventInterface
     */
    public function stopPropagation(bool $propagationStopped = true): EventInterface
    {
        $this->propagationStopped = $propagationStopped;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isPropagationStopped(): bool
    {
        return $this->propagationStopped;
    }

}
