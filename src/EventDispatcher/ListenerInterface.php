<?php

namespace Cbf\EventDispatcher;

/**
 * ListenerInterface interface.
 *
 * @package Cbf\EventDispatcher
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
interface ListenerInterface
{

    /**
     * @param EventInterface $event
     * @return bool
     */
    public function isSupported(EventInterface $event): bool;

}
