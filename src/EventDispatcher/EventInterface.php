<?php

namespace Cbf\EventDispatcher;

/**
 * EventInterface interface.
 *
 * @package Cbf\EventDispatcher
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
interface EventInterface
{

    /**
     * @return null|string
     */
    public function getName(): ?string;

    /**
     * @return bool
     */
    public function isPropagationStopped(): bool;

}
