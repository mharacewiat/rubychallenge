<?php

namespace Cbf\Registry;

use Cbf\Registry;

/**
 * ItemRegistryFactory class.
 *
 * @package Cbf\Registry
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class ItemRegistryFactory extends Registry
{

    /**
     * @return Registry
     */
    public static function create()
    {
        return new Registry([
            'VOUCHER' => ['Cabify Voucher', 5.0,],
            'TSHIRT' => ['Cabify T-Shirt', 20.0,],
            'MUG' => ['Cafify Coffee Mug', 7.5,],
        ]);
    }

}
