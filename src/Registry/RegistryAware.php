<?php

namespace Cbf\Registry;

use Cbf\RegistryInterface;

/**
 * RegistryAware trait.
 *
 * @package Cbf\Registry
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
trait RegistryAware
{

    /**
     * @var RegistryInterface
     */
    protected $registry;


    /**
     * @return RegistryInterface
     */
    public function getRegistry(): RegistryInterface
    {
        return $this->registry;
    }

}
