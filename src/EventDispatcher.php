<?php

namespace Cbf;

use Cbf\EventDispatcher\EventInterface;
use Cbf\EventDispatcher\ListenerInterface;

/**
 * Registry class.
 *
 * @package Cbf
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class EventDispatcher
{

    /**
     * @var EventDispatcher
     */
    protected static $instance;

    /**
     * @var iterable|ListenerInterface[]
     */
    protected $listeners = [];


    /**
     * EventDispatcher constructor.
     *
     * @see getInstance
     */
    protected function __construct()
    {
        /* Implements singleton - public construction restricted. */
    }


    /**
     * @param EventInterface $event
     * @return $this
     */
    public function dispatch(EventInterface $event)
    {
        foreach ($this->getListeners($event) as $listener) {
            try {
                $this->dispatchListener($event, $listener);
            } catch (\Exception $exception) {
                continue;
            }

            if (true == $event->isPropagationStopped()) {
                break;
            }
        }

        return $this;
    }

    /**
     * @param EventInterface $event
     * @param callable|ListenerInterface $listener
     * @throws \Exception
     */
    protected function dispatchListener(EventInterface $event, ListenerInterface $listener)
    {
        if (false == is_callable($listener)) {
            throw new \Exception(sprintf('Listener "%s" has to be callable. Implement __invoke() magic method to proceed.', get_class($listener)));
        }

        if (false == $listener->isSupported($event)) {
            throw new \Exception(sprintf('Event "%s" is not supported by listener "%s".', get_class($event), get_class($listener)));
        }

        call_user_func($listener, $event);

        return;
    }


    /**
     * @param string $event
     * @param ListenerInterface $listener
     * @return ListenerInterface
     */
    public function register(string $event, ListenerInterface $listener)
    {
        return $this->listeners[$event][] = $listener;
    }


    /**
     * @param EventInterface $event
     * @return iterable|ListenerInterface[]
     */
    protected function getListeners(EventInterface $event): iterable
    {
        return @$this->listeners[$event->getName() ?? get_class($event)] ?? [];
    }


    /**
     * @return EventDispatcher
     */
    public static function getInstance(): EventDispatcher
    {
        return (self::$instance ?? self::$instance = new self());
    }

}
