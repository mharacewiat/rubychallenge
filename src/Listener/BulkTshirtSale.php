<?php

namespace Cbf\Listener;

use Cbf\Event\GetTotal;
use Cbf\EventDispatcher\EventInterface;
use Cbf\EventDispatcher\ListenerInterface;

/**
 * BulkSale class.
 *
 * Check if checkout contains 3 or more t-shirt's. If so, reduce their price to 19.
 *
 * @package Cbf\Listener
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class BulkTshirtSale implements ListenerInterface
{

    /**
     * @param GetTotal $event
     */
    public function __invoke(GetTotal $event): void
    {
        $tshirtCount = $event->countItem('TSHIRT');

        if (3 <= $tshirtCount) {
            $event->substituteTotal($this->calculateSubstitution($tshirtCount));
        }

        return;
    }

    /**
     * @param int $count
     * @return float
     */
    protected function calculateSubstitution(int $count): float
    {
        return ($count * $this->getPriceDifference());
    }

    /**
     * @return float
     */
    protected function getPriceDifference(): float
    {
        return (20.0 - 19.0);
    }


    /**
     * {@inheritdoc}
     */
    public function isSupported(EventInterface $event): bool
    {
        return ($event instanceof GetTotal);
    }

}
