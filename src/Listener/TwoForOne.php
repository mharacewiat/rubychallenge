<?php

namespace Cbf\Listener;

use Cbf\Event\GetTotal;
use Cbf\EventDispatcher\EventInterface;
use Cbf\EventDispatcher\ListenerInterface;

/**
 * TwoForOne class.
 *
 * Reduce checkout total for voucher duplicates.
 *
 * @package Cbf\Listener
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class TwoForOne implements ListenerInterface
{

    /**
     * @param GetTotal $event
     */
    public function __invoke(GetTotal $event)
    {
        $voucherCount = $event->countItem('VOUCHER');

        if (1 < $voucherCount) {
            $event->substituteTotal($this->calculateSubstitution($voucherCount));
        }

        return;
    }

    /**
     * @param int $count
     * @return float
     */
    protected function calculateSubstitution(int $count): float
    {
        return (round(($count / 2), 0, PHP_ROUND_HALF_DOWN) * $this->getVoucherPrice());
    }

    /**
     * @return float
     */
    protected function getVoucherPrice(): float
    {
        return 5.0;
    }


    /**
     * {@inheritdoc}
     */
    public function isSupported(EventInterface $event): bool
    {
        return ($event instanceof GetTotal);
    }

}
