<?php

namespace Cbf;

/**
 * Checkout abstract class.
 *
 * @package Cbf
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class Checkout implements CheckoutInterface
{

    /**
     * @var iterable|string[]
     */
    protected $items = [];

    /**
     * @var float
     */
    protected $total = .0;


    /**
     * @return string
     */
    public function __toString(): string
    {
        return vsprintf("Items: %s\nTotal: %.2f €\n", [
            (($items = $this->getItems()) ? implode(', ', $items) : 'None'),
            $this->getTotal(),
        ]);
    }


    /**
     * {@inheritdoc}
     */
    public function scan(string $item): bool
    {
        return true;
    }

    /**
     * @return CheckoutInterface
     */
    public function clear(): CheckoutInterface
    {
        [$this->items, $this->total] = [[], .0];
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function substituteTotal(float $amount): CheckoutInterface
    {
        $this->total -= $amount;
        return $this;
    }


    /**
     * @codeCoverageIgnore
     *
     * {@inheritdoc}
     */
    public function getTotal(): float
    {
        return $this->total;
    }

    /**
     * @codeCoverageIgnore
     *
     * {@inheritdoc}
     */
    public function getItems(): iterable
    {
        return $this->items;
    }

}
