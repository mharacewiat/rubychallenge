<?php

namespace Cbf;

/**
 * RegistryInterface interface.
 *
 * @package Cbf
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
interface RegistryInterface
{

    /**
     * Get registry-stored data by given key.
     *
     * @param string $key
     * @return mixed
     */
    public function get(string $key);

}
