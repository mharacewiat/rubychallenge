<?php

namespace Cbf;

/**
 * Registry class.
 *
 * @package Cbf
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class Registry implements RegistryInterface
{

    /**
     * @var iterable
     */
    protected $items;


    /**
     * Registry constructor.
     *
     * @param iterable $items
     */
    public function __construct(iterable $items = [])
    {
        $this->items = $items;
    }


    /**
     * @param string $key
     * @return array|mixed
     * @throws \Exception
     */
    public function get(string $key)
    {
        if (false == array_key_exists($key, $this->items)) {
            throw new \Exception(sprintf('Item "%s" not registered', $key));
        }

        return $this->items[$key];
    }

}
