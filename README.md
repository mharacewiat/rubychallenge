About
---

Qualification test result for [Cabify Mobile Challenge](INSTRUCTIONS.md).

Assumptions:
- Test purpose is to proof programmer's PHP skills. That's why no 3-rd party library will be used (with except fo composer's autoloader and phpunit)
- Maintain as less as possible boilerplate code. Front-end is ignored since is not essential for this test
- Code will be self explanatory. No redundant documentation is needed. Process, however, needs to be described widely
- Personal aim to finish this in up to 3 hours. Finally test took 3:20 hours to deliver. Missing and planned implementations described below

Solution
---

Solution strictly follows interface from instructions.

Flexibility for checkout summary provided by implementation of EventDispatcher design pattern.
Every custom total adjustments can be done by implementing ListenerInterface and putting logic into it's __invoke() method.

Supported items list can be adjusted by overriding Registry class (for example - load them from database, file or cache).

Todo
---

- Fluent scan interface
- Item models
- More fault tolerant scan method (preferably using item models)
- EventDispatcher subscribers
- Decorator or presenter instead of __toString
- Explicit exceptions handling. Concrete exceptions classes and extended try-catch blocks
- Error handling and logging
- Make events or listeners avare of items prices from registry (now it's hardcoded in each listener)

Installation
---

```bash
git clone git@bitbucket.org:haru0/rubychallenge.git
composer install
```

Execute
---

```bash
php public/index.php
```

Tests
---

```bash
vendor/bin/phpunit --coverage-html tests/coverage
```

Author 
---

Michał Haracewiat <m.haracewiat@gmail.com>
