<?php

namespace Tests\Functional;

use Cbf\Checkout\AwareCheckout;
use Cbf\CheckoutInterface;
use Cbf\Event\GetTotal;
use Cbf\EventDispatcher;
use Cbf\Listener\BulkTshirtSale;
use Cbf\Listener\TwoForOne;
use Cbf\Registry\ItemRegistryFactory;
use PHPUnit\Framework\TestCase;

/**
 * CheckoutTest class.
 *
 * @package Tests\Functional
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class CheckoutTest extends TestCase
{

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        $eventDispatcher = EventDispatcher::getInstance();
        $eventDispatcher->register(GetTotal::class, new TwoForOne());
        $eventDispatcher->register(GetTotal::class, new BulkTshirtSale());
    }


    /**
     * @dataProvider checkoutData
     *
     * @param iterable|string[] $items
     * @param float $result
     */
    public function testCheckout(iterable $items, float $result): void
    {
        $checkout = $this->getCheckout();

        foreach ($items as $item) {
            $checkout->scan($item);
        }

        $this->assertSame($result, $checkout->getTotal());
    }


    /**
     * @see INSTRUCTIONS.md
     *
     * @return array
     */
    public function checkoutData(): array
    {
        return [
            [
                ['VOUCHER', 'TSHIRT', 'MUG'],
                32.5,
            ],
            [
                ['VOUCHER', 'TSHIRT', 'VOUCHER'],
                25.0,
            ],
            [
                ['TSHIRT', 'TSHIRT', 'TSHIRT', 'VOUCHER', 'TSHIRT'],
                81.0,
            ],
            [
                ['VOUCHER', 'TSHIRT', 'VOUCHER', 'VOUCHER', 'MUG', 'TSHIRT', 'TSHIRT'],
                74.5,
            ],
        ];
    }

    /**
     * @return CheckoutInterface
     */
    protected function getCheckout(): CheckoutInterface
    {
        return new AwareCheckout(ItemRegistryFactory::create());
    }

}
