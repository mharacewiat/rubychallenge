<?php

namespace Tests\Unit;

use Cbf\EventDispatcher;
use Cbf\EventDispatcher\EventInterface;
use PHPUnit\Framework\TestCase;

/**
 * EventDispatcherTest class.
 *
 * @package Tests\Unit
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class EventDispatcherTest extends TestCase
{

    public function testDispatch()
    {
        $this->markTestIncomplete();

        $event = $this->getMockForAbstractClass(EventInterface::class, [
            'getName',
        ]);
        $eventDispatcher = $this->getMockForAbstractClass(EventDispatcher::class, [], '', false);

        $event
            ->expects($this->any())
            ->method('getName')
            ->willReturn('qux');

        /**
         * @var EventInterface $event
         * @var EventDispatcher $eventDispatcher
         */

        $eventDispatcher->dispatch($event);
    }

}
