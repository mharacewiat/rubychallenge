<?php

namespace Tests\Unit;

use Cbf\Checkout;
use PHPUnit\Framework\TestCase;

/**
 * CheckoutTest class.
 *
 * @package Tests\Unit
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class CheckoutTest extends TestCase
{

    public function testToString()
    {
        $checkout = $this->createPartialMock(Checkout::class, [
            'getItems',
            'getTotal',
        ]);

        $checkout
            ->expects($this->any())
            ->method('getItems')
            ->willReturn([
                'Foo',
                'Bar',
                'Baz',
            ]);

        $checkout
            ->expects($this->any())
            ->method('getTotal')
            ->willReturn(10.0);

        /** @var Checkout $checkout */
        $string = <<< EOL
Items: Foo, Bar, Baz
Total: 10.00 €

EOL;
        $this->assertSame($string, strval($checkout));
    }

    public function testScan()
    {
        $checkout = $this->createPartialMock(Checkout::class, []);

        /** @var Checkout $checkout */

        $this->assertTrue($checkout->scan('foo'));
        $this->assertSame(.0, $checkout->getTotal());
    }

    public function testClear()
    {
        $checkout = $this->createPartialMock(Checkout::class, []);

        $itemsReflection = new \ReflectionProperty($checkout, 'items');
        $itemsReflection->setAccessible(true);
        $itemsReflection->setValue($checkout, [
            'foo',
            'bar',
            'baz',
        ]);

        $totalReflection = new \ReflectionProperty($checkout, 'total');
        $totalReflection->setAccessible(true);
        $totalReflection->setValue($checkout, .1);

        /** @var Checkout $checkout */

        $checkout->clear();

        $this->assertEmpty($checkout->getItems());
        $this->assertSame(.0, $checkout->getTotal());
    }

    public function testSubstituteTotal()
    {
        $checkout = $this->createPartialMock(Checkout::class, []);

        $totalReflection = new \ReflectionProperty($checkout, 'total');
        $totalReflection->setAccessible(true);
        $totalReflection->setValue($checkout, 10.0);

        /** @var Checkout $checkout */

        $checkout->substituteTotal(1.0);

        $this->assertSame(9.0, $checkout->getTotal());
    }

}
