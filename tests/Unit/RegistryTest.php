<?php

namespace Tests\Unit;

use Cbf\Registry;
use PHPUnit\Framework\TestCase;

/**
 * RegistryTest class.
 *
 * @package Tests\Unit
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class RegistryTest extends TestCase
{

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Item "qux" not registered
     */
    public function testGet()
    {
        $registry = $this->getMockForAbstractClass(Registry::class, []);

        $reflectionProperty = new \ReflectionProperty($registry, 'items');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($registry, [
            'foo' => ($foo = 1),
            'bar' => ($bar = 2),
            'baz' => ($baz = 3),
        ]);

        /** @var Registry $registry */

        $this->assertSame($foo, $registry->get('foo'));
        $this->assertSame($bar, $registry->get('bar'));
        $this->assertSame($baz, $registry->get('baz'));
        $registry->get('qux');
    }

}
